<?php
// ========== DB connection copied from oppgave 1
$dbuser = 'root';
$dbpwd  = '';

$dbname = 'IMT2291EksamenLosningsforslag';

try { // Try to connect to database'
  $db = new PDO("mysql:host=127.0.0.1;dbname=$dbname", $dbuser, $dbpwd);
} catch (PDOException $e) {	// If an error is detected
	if (isset($debug))		// If we are doing development
		die ('Unable to connect to database : '.$e->getMessage());
	else 					// Do NOT show above information to end users.
		die ('Unable to connect to database, please try again later');
}

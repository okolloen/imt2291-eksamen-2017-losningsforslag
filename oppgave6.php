<?php

require_once 'db.php';

$sql = 'SELECT id, uname, isnull(avatar) as avatarnull FROM user';
$stmt = $db->prepare($sql);
$stmt->execute (array());

$data = [];
while ($row=$stmt->fetch(PDO::FETCH_ASSOC)) {
  $data[] = array ('id'=>$row['id'], 
                  'uname'=>$row['uname'],
                  'avatar'=>$row['avatarnull']==0?'y':'n');
}

header ('Content-type: application/json');
echo json_encode($data);

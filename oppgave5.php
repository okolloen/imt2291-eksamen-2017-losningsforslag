<?php

require_once 'db.php';  // Connect to the database

$sql = 'SELECT id, uname, avatar FROM user ORDER BY uname';
$stmt = $db->prepare ($sql);
$stmt->execute (array()); // Get user id, user name and avatar

?>
<!DOCTYPE html>
<html lang="no">
  <head>
    <meta name="viewport" content="width=device-width">
    <meta charset="utf-8">
    <title>Oppgave5</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
          crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
          crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-xs-2"><b>User id</b></div>
        <div class="col-xs-6"><b>User name</b></div>
        <div class="col-xs-4"><b>Avatar</b></div>
      </div>
      <?php
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
          echo '<div class="row">';
          echo '<div class="col-xs-2">'.$row['id'].'</div>';
          echo '<div class="col-xs-6">'.$row['uname'].'</div>';
          if ($row['avatar']!=null) {   // User has avatar image
            echo '<div class="col-xs-4"><img class="img-responsive" src="avatar.php?id='.$row['id'].'"></div>';
          } else {                      // User has no avatar image
            echo '<div class="col-xs-4"></div>';
          }
          echo "</div>\n";
        }
       ?>
    </div>
  </body>
</html>

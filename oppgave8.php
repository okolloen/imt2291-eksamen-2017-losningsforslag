<!DOCTYPE html>
<html lang="no">
  <head>
    <meta name="viewport" content="width=device-width">
    <meta charset="utf-8">
    <title></title>
    <style>
      td.semester {
        width: 50px;
        text-align: right;
      }

      td.name {
        width: 300px;
      }

      td.type {
        width: 40px;
        text-align: center;
      }

      td.code {
        width: 100px;
      }

      table {
        border-spacing: 0;
        border-collapse: collapse;
      }

      tbody td {
        border-bottom: 1px solid grey;
      }
    </style>
  </head>
  <body>
<?php

require_once 'db.php';  // Connect to the database

// Select relevant fields from relevant tables.
// Connect studyprogram to studyprogramContent and studyprogramContent to subject
// then limit to given start year and subject to the ones running in relevant
// year given start year and semester.
// The last part gives 0 for semester 1, 1 for semester 2 and 3, 2 for semester 4 and 5
// and 3 for semester 6 (added to start year to find the year we want the subject to be given)
$sql = "
SELECT studyprogram.name as studyprogram, studyprogramContent.semester, subject, type, credits, subject.name, url
FROM studyprogram, studyprogramContent, subject
WHERE studyprogram.id=studyprogramContent.studyprogram
AND subject=code
AND startYear=?
AND year=startYear+(studyprogramContent.semester-(studyprogramContent.semester)%2)/2
ORDER BY studyprogram.name, studyprogramContent.semester, subject";

$stmt = $db->prepare ($sql);
$stmt->execute (array (2016));
$data = $stmt->fetchAll(PDO::FETCH_ASSOC);
echo "<h1>{$data[0]['studyprogram']}</h1>\n";
echo "<table><thead><tr><th>Emnekode</th><th>Emnenavn</th><th>O/V</th><th colspan='6'>Studiepoeng pr semester</th></tr>\n";
echo "<tr><th colspan='3'></th><th class='semester'>S1(h)</th><th class='semester'>S2(v)</th><th class='semester'>S3(h)</th><th class='semester'>S4(v)</th><th class='semester'>S5(h)</th><th class='semester'>S6(v)</th></tr></thead>\n";
echo "<tbody>\n";
foreach ($data as $subject) {   // Go through all subjects
  $subject['name'] = utf8_encode ($subject['name']);
  echo "<tr><td class='code'>{$subject['subject']}</td><td class='name'>{$subject['name']}</td>";
  echo '<td class="type">'.(strcmp($subject['type'],'obligatory')==0?'O':'V').'</td>';
  for ($semester=1; $semester<7; $semester++) { // Find what semester this subject is being lectured
    echo '<td class="semester">';
    echo $subject['semester']*1==$semester?$subject['credits']:'';
    echo '</td>';
  }
  echo "</tr>\n";
}
?>
  </body>
</html>

<?php
session_start();

// ========== DB connection copied from oppgave 1
$dbuser = 'root';
$dbpwd  = '';

$dbname = 'IMT2291EksamenLosningsforslag';

try { // Try to connect to database'
  $db = new PDO("mysql:host=127.0.0.1;dbname=$dbname", $dbuser, $dbpwd);
} catch (PDOException $e) {	// If an error is detected
	if (isset($debug))		// If we are doing development
		die ('Unable to connect to database : '.$e->getMessage());
	else 					// Do NOT show above information to end users.
		die ('Unable to connect to database, please try again later');
}
// ========

if (isset($_POST['pwd'])) { // User attempts to log in
  $sql = 'SELECT id, pwd FROM user WHERE uname=?';
  $stmt = $db->prepare ($sql);
  $stmt->execute (array ($_POST['uname']));
  if ($user = $stmt->fetch(PDO::FETCH_ASSOC)) {         // Found user
    if (password_verify($_POST['pwd'],$user['pwd'])) {  // Correct pwd
      $_SESSION['uid'] = $user['id'];
      $_SESSION['user'] = $_POST['uname'];
    } else {                                            // Wrong pwd
      $unknownUserNamePwd = true;
    }
  } else {                                              // Unknown user
    $unknownUserNamePwd = true;
  }
} else if (isset($_POST['logout'])) {                   // User is logging out
  unset ($_SESSION['uid']);
  unset ($_SESSION['user']);
  session_destroy();
}
?>
<!DOCTYPE html>
<html lang="no">
  <head>
    <meta name="viewport" content="width=device-width">
    <meta charset="utf-8">
    <title>Oppgave 2</title>
    <style media="screen">
      label {
        display: inline-block;
        width: 80px;
      }
      form#login {
        border: 2px solid grey;
        padding: 20px;
        width: 220px;
      }
    </style>
  </head>
  <body>
    <?php
    if (isset($_SESSION['uid'])) {  // User is logged in ?>
      <h1>Velkommen <?php echo $_SESSION['user']; ?><h1>
      <form action="oppgave2.php" method="post">
        <input type="submit" value="Logg ut" name="logout">
      </form>
    <?php } else {                  // User is not logged in?>
    <form action="oppgave2.php" method="post" id="login">
      <label for="uname">Brukernavn</label>
      <input type="text" name="uname" id="uname" <?php // Add username if failed login
        echo isset($_POST['uname'])?' value="'.$_POST['uname'].'"':''; ?>><br/>
      <label for="pwd">Passord</label>
      <input type="password" name="pwd" value="" id="pwd"><br/>
      <?php   // Show error message if wrong username/pwd
        echo isset($unknownUserNamePwd)?'Feil brukernavn/passord<br/>':'';
       ?>
      <input type="submit" name="submit" value="Logg på">
    </form>
    <?php } ?>
  </body>
</html>

<?php
session_start();

// ========== DB connection copied from oppgave 1
$dbuser = 'root';
$dbpwd  = '';

$dbname = 'IMT2291EksamenLosningsforslag';

try { // Try to connect to database'
  $db = new PDO("mysql:host=127.0.0.1;dbname=$dbname", $dbuser, $dbpwd);
} catch (PDOException $e) {	// If an error is detected
	if (isset($debug))		// If we are doing development
		die ('Unable to connect to database : '.$e->getMessage());
	else 					// Do NOT show above information to end users.
		die ('Unable to connect to database, please try again later');
}
// ========

if (isset($_POST['uname'])) {   // Create new user
  $sql = 'INSERT INTO user (uname, pwd) VALUES (?, ?)';
  $stmt = $db->prepare ($sql);
  $rows = $stmt->execute (array($_POST['uname'], password_hash($_POST['pwd'], PASSWORD_DEFAULT)));
  if ($rows==1) {               // New user created
    $_SESSION['user'] = $_POST['uname'];
    $_SESSION['uid'] = $db->lastInsertId();
    header('Location: oppgave2.php');
    die();
  }
}
?>
<!DOCTYPE html>
<html lang="no">
  <head>
    <meta name="viewport" content="width=device-width">
    <meta charset="utf-8">
    <title>oppgave3</title>
    <style media="screen">
      label {
        display: inline-block;
        width: 120px;
      }
      form {
        border: 2px solid grey;
        padding: 20px;
        width: 260px;
      }
    </style>
  </head>
  <body>
    <form action="oppgave3.php" id="newUser" method="post">
      <label for="uname">Brukernavn</label>
      <input type="text" name="uname" id="uname" <?php // If user exists
        echo isset($_POST['uname'])?' value="'.$_POST['uname'].'" ':''
       ?>required pattern="[a-zæøå]{5,32}"><br/>
      <label for="pwd">Passord</label>
      <input type="password" name="pwd" id="pwd" required pattern=".{8,}"><br/>
      <label for="pwd1">Bekreft passord</label>
      <input type="password" name="pwd1" id="pwd1" required pattern=".{8,}"><br/>
      <?php                                            // If user exists
        echo isset($_POST['uname'])?'Denne brukeren eksisterer allerede<br/>':'';
       ?>
      <input type="submit" name="submit" value="Opprett bruker">
    </form>
  </body>
  <script>
    var uname = document.getElementById('uname');
    uname.oninvalid = function(event) {   // Enhanced error message for username
      event.target.setCustomValidity('Brukernavn skal kun bestå av små bokstaver (fem til 32 tegn).');
    }

    var pwd = document.getElementById('pwd');
    pwd.oninvalid = function(event) {     // Enhanced error message for password
      event.target.setCustomValidity('Passord må være minst åtte tegn.');
    }

    var pwd1 = document.getElementById('pwd1');
    pwd1.oninvalid = function(event) {    // Enhanced error message for password
      event.target.setCustomValidity('Passord må være minst åtte tegn.');
    }

    var form = document.getElementById('newUser');
    newUser.addEventListener('submit', function (e) { // Check that both passwords are equal
      if (pwd.value!=pwd1.value) {
        e.preventDefault();
        alert('De to passordene må være like.');
      }
    });

  </script>
</html>

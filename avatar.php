<?php

require_once 'db.php';

$sql = 'SELECT avatar FROM user WHERE id=?';
$stmt = $db->prepare ($sql);
$stmt->execute (array($_GET['id']));

if ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
  header ("Content-type: image/jpg");
  echo $row['avatar'];
}

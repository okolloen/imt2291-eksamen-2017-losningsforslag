<?php
session_start();

if (!isset($_SESSION['uid'])) {   // Only allow for logged in users
  die ("Du har ikke tilgang til å gjøre dette.");
}
if (isset($_FILES['file'])) {     // File has been uploaded
  require_once 'db.php';  // Connect to database
  $content = file_get_contents($_FILES['file']['tmp_name']);

  $maxHeight = 150;
  $maxWidth = 150;

  // Image scaling stuff, from lecture notes
  $img = imagecreatefromstring($content);
  $width = imagesx($img);
  $height = imagesy($img);
  $scaleX = $width/$maxWidth;
  $scaleY = $height/$maxHeight;

  $scale = ($scaleX>$scaleY)?$scaleX:$scaleY;
  $scale = ($scale>1)?$scale:1;

  $scaled = imagecreatetruecolor($width/$scale, $height/$scale);
  imagecopyresampled($scaled, $img, 0, 0, 0, 0, $width/$scale, $height/$scale, $width, $height);

  ob_start();
  imagejpeg ($scaled, null, 100);
  $scaled = ob_get_contents();
  ob_end_clean();

  $sql = 'UPDATE user set avatar=? WHERE id=?';
  $sth = $db->prepare($sql);
  $sth->execute(array($scaled, $_SESSION['uid']));

  // Show new avatar image, use base64 encoding to insert image data directly into document
  echo "Ditt nye avatarbilde : <img src='data:image/jpeg;base64, ".base64_encode($scaled)."'>";
  die();  // Don't show the form if image is uploaded.
}
?>
<!DOCTYPE html>
<html lang="no">
  <head>
    <meta name="viewport" content="width=device-width">
    <meta charset="utf-8">
    <title>Oppgave 4</title>
  </head>
  <body>
    <form action="oppgave4.php" method="post" enctype="multipart/form-data">
      <label for="img">Nytt avatarbilde</label>
      <input type="file" name="file"><br>
      <input type="submit" name="submit" value="Lagre nytt avatarbilde">
    </form>
  </body>
</html>

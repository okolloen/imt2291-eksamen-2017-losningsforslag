<!DOCTYPE html>
<html lang="no">
  <head>
    <meta name="viewport" content="width=device-width">
    <meta charset="utf-8">
    <title>Oppgave 8</title>
  </head>
  <body>
    <table>
      <th>Postnummer</th>
      <th>sted</th>
    <?php
    // Read data from URL
    $jsonString = file_get_contents('http://api.geonames.org/postalCodeSearchJSON?placename=gj%C3%B8vik&country=NO&maxRows=30&username=okolloen');
    // Decode data as json data
    $jsonData = json_decode ($jsonString);

    // Go through the array of postal codes
    foreach ($jsonData->postalCodes as $postal) {
      if ($postal->adminName1=='Oppland') { // If place is in Oppland
        echo "<tr><td>{$postal->postalCode}</td><td>{$postal->placeName}</td></tr>";
      }
    }
    ?>
  </body>
</html>

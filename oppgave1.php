<?php

$dbuser = 'root';
$dbpwd  = '';

$dbname = 'IMT2291EksamenLosningsforslag';

try { // Try to connect to database'
  $db = new PDO('mysql:host=127.0.0.1;dbname=', $dbuser, $dbpwd);
} catch (PDOException $e) {	// If an error is detected
	if (isset($debug))		// If we are doing development
		die ('Unable to connect to database : '.$e->getMessage());
	else 					// Do NOT show above information to end users.
		die ('Unable to connect to database, please try again later');
}

$sql = "CREATE DATABASE IF NOT EXISTS $dbname";
$db->exec ($sql);       // Create database
$sql = "USE $dbname";
$db->exec ($sql);       // Use newly created database

// SQL to create the table as specified in "oppgave 1".
$sql = "CREATE TABLE IF NOT EXISTS user (
          id BIGINT NOT NULL AUTO_INCREMENT,
          uname VARCHAR(32) NOT NULL,
          pwd VARCHAR(255) NOT NULL,
          avatar BLOB NULL,
          PRIMARY KEY (id),
          UNIQUE (uname))";
$rows = $db->exec ($sql);

// Since we could connect to the database the rest should always work.
echo 'Databasen og tabellen er opprettet.';

$hashedPwd = password_hash('test', PASSWORD_DEFAULT); // Create hash of the password

$sql = 'INSERT INTO user (uname, pwd) values (?, ?)';
$stmt = $db->prepare ($sql);
$stmt->execute (array ('test', $hashedPwd));          // Add user to table

echo 'User "test" added to table "user"';
